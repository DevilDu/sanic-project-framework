"""
Author: Gaoqiang Du
Description：仿照drf的serializer写的简易版序列化器
Time: 2022/2/17 9:34
"""


class ReadOnlyModelSer:

    def __init__(self, instance=None, many=False, *args, **kwargs):
        self.instance = instance
        self.many = many

    async def to_representation(self, instance):
        res = {}
        for field in self.Meta.fields:
            res[field] = getattr(instance, field)

        return res

    @property
    async def data(self):
        if self.many:
            d = []
            for i in self.instance:
                res = await self.to_representation(i)
                d.append(res)

            return d
        return await self.to_representation(self.instance)

    class Meta:
        fields = []
