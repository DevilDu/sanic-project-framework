from typing import Dict, Optional

from sanic.response import HTTPResponse

from utils.common import SampleJsonEncoder, json

"""
Author: Gaoqiang Du
Description：通用的业务响应方法
Date: 2022/2/5 12:56
"""


def general_json_res(
        status: int = 200,
        msg: str = "OK.",
        http_status: int = 200,
        headers: Optional[Dict[str, str]] = None,
        **kwargs
) -> HTTPResponse:
    # According to business requirements, rewrite response
    data = {'status': status, 'msg': msg}
    if kwargs:
        data["data"], kwargs = kwargs, {}

    return HTTPResponse(
        json.dumps(data, cls=SampleJsonEncoder, **kwargs),
        headers=headers,
        status=http_status,
        content_type="application/json",
    )
