import multiprocessing
import platform
from importlib import import_module

from apps import create_app

SETTINGS_MODULE = "conf.prod"
Config = import_module(SETTINGS_MODULE)

app = create_app(Config)


# `workers` is num of subprocesses
def get_workers():
    if not app.config.DEBUG and platform.system() != "Windows":
        return multiprocessing.cpu_count()
    return 1


if __name__ == '__main__':
    app.run(
        host="0.0.0.0",
        debug=app.config.DEBUG,
        access_log=app.config.ACCESS_LOG,
        workers=get_workers()
    )
