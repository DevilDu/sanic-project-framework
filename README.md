# Sanic项目框架

#### 背景

此项目可帮助Sanic开发者快速搭建自己的项目，而免去前期各种配置的烦恼，同时还附赠可能会帮到你的通用方法（雪花算法、唯一码）。

#### 软件架构

* 语言：python 3.7+
* 框架：Sanic
* 数据库：MySQL 5.6
* 缓存：Redis



#### 使用说明

请确保Python版本在3.7及以上，因为从3.7开始新增关键词`async`，支持我们进行异步操作。

如果调用的库都是同步代码，可使用`utils.common.sync2async`封装下，实现异步调用。

配置完数据库后，请查看`aerich`文档生成数据库表，或者将`tortoise`配置中的`generate_schemas`改为True自动生成表。

如果使用`Dockerfile`和`docker-compose`的话请修改其内部的配置。

#### 参考文档

[Sanic 框架](https://sanic.dev/zh/)

[tortoise-orm](https://tortoise-orm.readthedocs.io/en/latest/)

[aerich](https://github.com/tortoise/aerich)

[aioredis](https://aioredis.readthedocs.io/en/latest/migration/)

[Docker](http://www.imooc.com/wiki/dockerlesson)

[Docker Compose](https://docs.docker.com/compose/)
