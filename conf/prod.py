import os

import sentry_sdk
from sentry_sdk.integrations.sanic import SanicIntegration

from .base import *  # noqa

DEBUG = False
ACCESS_LOG = False

DB_URL = os.getenv(
    "DB_URL",
    "mysql://root:password@host:3306/db_name?charset=utf8mb4")

REDIS_URL = os.getenv("REDIS_URL", "redis://127.0.0.1:6379")

TORTOISE_ORM = {
    "connections": {"default": DB_URL},
    "apps": {
        "models": {
            "models": ["aerich.models"] + [app + ".models" for app in APPLICATIONS],  # noqa
            "default_connection": "default",
        },
    },
}

# start sentry
sentry_sdk.init(
    dsn="http://****",
    integrations=[SanicIntegration()],
    environment="production",
    traces_sample_rate=0.5,
    send_default_pii=True
)
