from datetime import timedelta

from tortoise import Model, fields


class DemoUser(Model):
    name = fields.CharField(50)

    is_del = fields.BooleanField(default=False, description="tombstoned")
    create_time = fields.DatetimeField(auto_now_add=True)
    update_time = fields.DatetimeField(auto_now=True)

    def __str__(self):
        return f"I am {self.name}"


class Project(Model):
    user = fields.ForeignKeyField("models.DemoUser")
    title = fields.CharField(255)
    description = fields.TextField(max_length=510)

    durations = fields.TimeDeltaField(default=timedelta(3))

    is_del = fields.BooleanField(default=False, description="tombstoned")
    create_time = fields.DatetimeField(auto_now_add=True)
    update_time = fields.DatetimeField(auto_now=True)
