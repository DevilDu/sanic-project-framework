from sanic import Blueprint

from . import views

bp = Blueprint("demo", url_prefix="/demo")

# load blueprint request middleware
# from utils import middlewares
# bp.on_request(middlewares.check_user)

bp.add_route(views.UserView.as_view(), "user/<pk:int>")
bp.add_route(views.UserView.as_view(), "user/")
bp.add_route(views.CacheView.as_view(), "cache/<key:str>")
