from utils.serializer import ReadOnlyModelSer

"""
Author: Gaoqiang Du
Description：序列化组件使用示例
"""


class DemoUserSer(ReadOnlyModelSer):
    class Meta:
        fields = ["id", "name", ]
